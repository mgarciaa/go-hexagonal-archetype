## General info
Go Backend archetype 

## Description
REST Api developed with golang 
It is an example to follow if you want to build an api rest with hexagonal architecture 

To change database name or something else, please go to the specific entity.

## Technologies
Project is created with:
* Go version: 1.16.3
* Echo framework
* Docker version: 20.10.2
* Mongo official library for go MongoDB
* Mongo DB
* Swagger for go swago

## Setup
* To run this project, you must have Docker and docker-compose installed on your machine.

* Pull or download the project, go to the terminal and follow the steps below:

```
$ cd <repository_folder>
$ docker-compose up -d
```
You have to see at your terminal something like this

```
Creating mongo-tangelo ... done
Creating tangelo_go_api ... done

```
To check the containers are running you can type

```
$ docker ps
```
You have to see something similar to this

```
CONTAINER ID   IMAGE                                COMMAND                  CREATED              STATUS              PORTS                               NAMES
cc4eef02de1f   tangelo_api_flatten_tangelo_go_api   "/app/main"              About a minute ago   Up About a minute   0.0.0.0:8081->8081/tcp              tangelo_go_api
b8d16eb1b0c2   mongo:latest                         "docker-entrypoint.s…"   About a minute ago   Up About a minute   0.0.0.0:27017->27017/tcp            mongo-tangelo
```

If everything went well, you are now running the API REST and you can go directly go to your browser and type

```
http://localhost:8081/swagger/index.html
```






