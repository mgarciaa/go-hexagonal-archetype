package utilsFunctions

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestFlatArr(t *testing.T) {

	inputArray := []int{1, 2, 3}
	stack := []interface{}{}
	index := 0
	val := reflect.ValueOf(inputArray)
	flArr := FlatArr(stack, index, val)
	assert.Equal(t, flArr, []interface{}([]interface{}{1, 2, 3}), "The two arrays should be the same.")
}

func TestSearchMaxIntOfMap(t *testing.T) {

	mapDepth := make(map[int]int)
	mapDepth[1] = 1
	mapDepth[2] = 2
	mapDepth[3] = 3
	maxDepth := SearchMaxIntOfMap(mapDepth)
	assert.Equal(t, maxDepth, 3,"should be 3" )
}
