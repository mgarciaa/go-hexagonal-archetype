package utilsFunctions

import (
	"reflect"
)

func FlatArr(stack []interface{}, index int, arrType reflect.Value) []interface{} {

	if arrType.Kind() == reflect.Interface {
		arrType = arrType.Elem()
	}

	if arrType.Kind() == reflect.Array || arrType.Kind() == reflect.Slice {
		for i := 0; i < arrType.Len(); i++ {
			stack = FlatArr(stack, index, arrType.Index(i))
		}
	} else {
		stack = append(stack, arrType.Interface())
	}
	return stack
}

func MaxDepthOfArray(arrayInterface interface{}, arrCount int, mapDepth map[int]int) map[int]int {

	arrayInterface2 := arrayInterface.([]interface{})
	for i := 0; i < len(arrayInterface2); i++ {
		if reflect.TypeOf(arrayInterface2[i]).Kind() == reflect.Slice || reflect.TypeOf(arrayInterface2[i]).Kind() == reflect.Array {
			arrCount++
			mapDepth[i] = arrCount
			s := reflect.ValueOf(arrayInterface2[i])
			ret := make([]interface{}, s.Len())
			for i := 0; i < s.Len(); i++ {
				ret[i] = s.Index(i).Interface()
			}
			MaxDepthOfArray(ret, arrCount, mapDepth)
		}
	}
	return mapDepth
}

func SearchMaxIntOfMap(mapDepth map[int]int) (maxDepth int){
	maxDepth = mapDepth[0]
	for _, value := range mapDepth {
		if value > maxDepth {
			maxDepth = value
		}
	}
	return
}
