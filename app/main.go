package main

import (
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/swaggo/echo-swagger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"net/http"
	_ "tangelo_api_flatten/app/docs"
	"tangelo_api_flatten/app/domain/usecases/array"
	arrays2 "tangelo_api_flatten/app/infrastructure/db/mongo/arrays"
	arrays "tangelo_api_flatten/app/infrastructure/web/rest/v1/arrays_handlers"
	"time"
)



// @title Tangelo api flatten
// @description Tangelo api flatten
// @version 1.0
// @host localhost:8081
// @BasePath /api/v1
func main() {

	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.CORS())
	e.GET("/swagger/*", echoSwagger.WrapHandler)
	setupDependencies(e)

	// HealthCheck godoc
	// @Summary Show the status of server.
	// @Description get the status of server.
	// @Tags root
	// @Accept */*
	// @Produce json
	// @Success 200 {object} map[string]interface{}
	// @Router / [get]
	e.GET("/", HealthCheck)

	e.Logger.Fatal(e.Start(":8081"))


}
func HealthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": "Server is up and running",
	})
}

func setupDependencies(e *echo.Echo){
	g := e.Group("/api/v1")

	mongoClient := getMongoClient()
	arrayRepository := arrays2.NewArrayMongoRepository(mongoClient)
	useCases := array.NewArrayUsesCases(arrayRepository)
	_ = arrays.NewArrayHandler(g,useCases)
}

func getMongoClient() *mongo.Client {

	const URI = "mongodb://tangelo_admin:tangeloapi@mongo-tangelo:27017/?connect=direct"

	client, err := mongo.NewClient(options.Client().ApplyURI(URI))
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer cancel()

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	databases, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Success connection",databases)
	return client
}