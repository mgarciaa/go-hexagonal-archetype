package arrays

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"tangelo_api_flatten/app/domain/models"
)

// CreateTodo godoc
// @Summary Flat an specific array
// @Description Flat an specific array
// @Tags Flat an array
// @Accept json
// @Produce json
// @Param incoming_array body models.InputArray true "incoming_array"
// @Success 200
// @Failure 400
// @Router /arrays/flatten [post]
func (h *arrayHandler) FlatArray(c echo.Context) error {

	var unFlatArray models.InputArray
	if err := c.Bind(&unFlatArray); err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{"message":"It must be an array"})
	}
	input := unFlatArray.IncomingArray
	flattenArray, depth := h.arrayUseCase.FlatArray(input)
	return c.JSON(http.StatusOK, echo.Map{"flattened": flattenArray, "depth": depth})
}
