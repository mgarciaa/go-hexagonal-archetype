package arrays

import (
	"bytes"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	responseFlat     = "{\"depth\":0,\"flattened\":[1,2,3]}\n"
	responseFlatError = "{\"message\":\"It must be an array\"}\n"

)

func TestArrayHandler_FlatArray(t *testing.T) {
	t.Parallel()
	t.Run("FlatArray function successful", func(t *testing.T) {

		usecase := new(arrayUseCaseMock)
		usecase.On("FlatArray",mock.Anything).Return(getFlatArraysStub(), 1)

		e := echo.New()
		req := httptest.NewRequest(http.MethodPost, "/arrays/flatten", bytes.NewBufferString("{\"incoming_array\": [1,2,[3]]}"))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		resp := httptest.NewRecorder()

		echoContext := e.NewContext(req, resp)
		g := e.Group("api/v1")
		h := NewArrayHandler(g, usecase)

		h.FlatArray(echoContext)

		assert.Equal(t, responseFlat, resp.Body.String())
		assert.Equal(t, http.StatusOK, resp.Code)
	})

	t.Run("FlatArray function failed", func(t *testing.T) {

		usecase := new(arrayUseCaseMock)
		usecase.On("FlatArray").Return(nil,0)

		e := echo.New()
		requestBody := getFlatArraysBody()
		requestBytes, _ := json.Marshal(requestBody)
		req := httptest.NewRequest(http.MethodPost, "/arrays/flatten", bytes.NewReader(requestBytes))
		resp := httptest.NewRecorder()

		echoContext := e.NewContext(req, resp)
		g := e.Group("api/v1")
		h := NewArrayHandler(g, usecase)

		h.FlatArray(echoContext)

		assert.Equal(t, responseFlatError, resp.Body.String())
		assert.Equal(t, http.StatusBadRequest, resp.Code)
	})

}