package arrays

import (
	"github.com/labstack/echo/v4"
	"tangelo_api_flatten/app/domain/usecases"
)

type arrayHandler struct {
	arrayUseCase usecases.ArrayUseCase
}

func NewArrayHandler(g *echo.Group, arrayUseCase usecases.ArrayUseCase) *arrayHandler {
	h := &arrayHandler{arrayUseCase: arrayUseCase}
	arraysGroup := g.Group("/arrays/flatten")
	arraysGroup.POST("", h.FlatArray)
	arraysGroup.GET("", h.LastHundredFlattenedArrays)
	return h
}



