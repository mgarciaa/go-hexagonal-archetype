package arrays

import (
	"errors"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	responseFlattened     = "{\"arrays\":[{\"incoming_array\":[1,2,3],\"flattened_array\":[1,2,[3,4]],\"depth\":1,\"date\":\"2021-11-17T20:34:58.651387237Z\"}]}\n"
	responseFlattenedError = "{\"error\":\"error\"}\n"

)

func TestArrayHandler_LastHundredFlattenedArrays(t *testing.T) {
	t.Parallel()
	t.Run("LastHundredFlattenedArrays function successful", func(t *testing.T) {

		usecase := new(arrayUseCaseMock)
		usecase.On("LastHundredFlattenedArrays").Return(getLastHundredFlattenedArraysStub(), nil)

		e := echo.New()
		req := httptest.NewRequest(http.MethodGet, "/arrays/flatten", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		resp := httptest.NewRecorder()

		echoContext := e.NewContext(req, resp)
		g := e.Group("api/v1")
		h := NewArrayHandler(g, usecase)

		h.LastHundredFlattenedArrays(echoContext)

		assert.Equal(t, responseFlattened, resp.Body.String())
		assert.Equal(t, http.StatusOK, resp.Code)
	})

	t.Run("LastHundredFlattenedArrays function failed", func(t *testing.T) {

		usecase := new(arrayUseCaseMock)
		usecase.On("LastHundredFlattenedArrays").Return(nil, errors.New("error"))

		e := echo.New()
		req := httptest.NewRequest(http.MethodGet, "/arrays/flatten", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		resp := httptest.NewRecorder()

		echoContext := e.NewContext(req, resp)
		g := e.Group("api/v1")
		h := NewArrayHandler(g, usecase)

		h.LastHundredFlattenedArrays(echoContext)

		assert.Equal(t, responseFlattenedError, resp.Body.String())
		assert.Equal(t, http.StatusInternalServerError, resp.Code)
	})

}
