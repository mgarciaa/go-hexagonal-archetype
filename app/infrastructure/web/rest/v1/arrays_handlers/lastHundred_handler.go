package arrays

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

// CreateTodo godoc
// @Summary Get last hundred request
// @Description Get last hundred request
// @Tags Last hundred request
// @Accept json
// @Produce json
// @Success 200
// @Failure 500
// @Router /arrays/flatten [get]
func (h *arrayHandler) LastHundredFlattenedArrays(c echo.Context) error {

	arr, err := h.arrayUseCase.LastHundredFlattenedArrays()
	if err != nil{
		return c.JSON(http.StatusInternalServerError, echo.Map{"error": "error"})
	}
	return c.JSON(http.StatusOK, echo.Map{"arrays": arr})
}
