package arrays

import (
	"github.com/stretchr/testify/mock"
	"tangelo_api_flatten/app/domain/models"
	"time"
)

type arrayUseCaseMock struct {
	mock.Mock
}

func (m *arrayUseCaseMock) FlatArray(inputArray interface{}) ([]interface{}, int) {
	args := m.Called(inputArray)
	if args.Get(0) == nil {
		return nil, 0
	}
	d := []interface{}{1,2,3}
	return d,0
}

func (m *arrayUseCaseMock) LastHundredFlattenedArrays() ([]*models.Array, error) {
	args := m.Called()
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]*models.Array), args.Error(1)
}

func getFlatArraysBody() *models.InputArray {

	unFlatArray := new(models.InputArray)
	unFlatArray.IncomingArray = []interface{}{1, 2, 3}
	return unFlatArray
}

func getFlatArraysStub() interface{} {

	x := make([]interface{},0)
	x = append(x,1,2,3,4 )
	return x
}

func getLastHundredFlattenedArraysStub() []*models.Array {
	arrs := make([]*models.Array, 0)
	arrs = append(arrs, getFlattenedArrayStub())
	return arrs
}

func getFlattenedArrayStub() *models.Array {

	inArr := []int{1, 2, 3}
	flatArr := []interface{}{1, 2, []int{3, 4}}
	clock := time.Date(
		2021, 11, 17, 20, 34, 58, 651387237, time.UTC)

	return &models.Array{
		IncomingArray:  inArr,
		FlattenedArray: flatArr,
		Depth:          1,
		Date:           clock,
	}
}