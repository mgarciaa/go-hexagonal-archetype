package arrays

import "go.mongodb.org/mongo-driver/mongo"

type arrayMongoRepository struct {
	mongoClient *mongo.Client
}

func NewArrayMongoRepository(mongoClient *mongo.Client) *arrayMongoRepository {
	return &arrayMongoRepository{mongoClient: mongoClient}
}

func (r *arrayMongoRepository) getCollection() *mongo.Collection {
	collection := r.mongoClient.Database("tangelo_flatten_array").Collection("arrays")
	return collection
}