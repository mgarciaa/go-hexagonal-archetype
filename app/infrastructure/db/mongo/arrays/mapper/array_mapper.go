package mapper

import (
	"tangelo_api_flatten/app/domain/models"
	"tangelo_api_flatten/app/infrastructure/db/mongo/arrays/entity"
)

func ArrayEntityToArrayModel(arrayEnEntity *entity.ArrayEntity) *models.Array {
	bitcoin := &models.Array{
		IncomingArray:  arrayEnEntity.IncomingArray,
		FlattenedArray: arrayEnEntity.FlattenedArray,
		Depth:          arrayEnEntity.Depth,
		Date:           arrayEnEntity.Date,
	}
	return bitcoin
}


func ArrayModelToArrayEntity(arrayModel *models.Array) *entity.ArrayEntity {
	array := &entity.ArrayEntity{
		IncomingArray:  arrayModel.IncomingArray,
		FlattenedArray: arrayModel.FlattenedArray,
		Depth:          arrayModel.Depth,
		Date:           arrayModel.Date,
	}
	return array
}