package entity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ArrayEntity struct {
	ID             primitive.ObjectID `bson:"id,omitempty"`
	IncomingArray  interface{}        `bson:"incoming_array"`
	FlattenedArray []interface{}      `bson:"flattened_array"`
	Depth          int                `bson:"depth"`
	Date           time.Time          `bson:"date"`
}
