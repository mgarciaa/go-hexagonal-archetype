package arrays

import (
	"context"
	"github.com/labstack/gommon/log"
	"tangelo_api_flatten/app/domain/models"
	"tangelo_api_flatten/app/infrastructure/db/mongo/arrays/mapper"
)

func (r *arrayMongoRepository) SaveFlattenRequest(array *models.Array) error {
	collection := r.getCollection()
	arrayEntity := mapper.ArrayModelToArrayEntity(array)
	_, err := collection.InsertOne(context.Background(), arrayEntity)
	if err != nil {
		log.Error(err)
		return err
	}
	return nil
}
