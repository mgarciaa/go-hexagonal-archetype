package arrays

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"tangelo_api_flatten/app/domain/models"
	"tangelo_api_flatten/app/infrastructure/db/mongo/arrays/entity"
	"tangelo_api_flatten/app/infrastructure/db/mongo/arrays/mapper"
)

func (r *arrayMongoRepository) GetLastHundredRequest() ([]*models.Array, error) {
	collection := r.getCollection()

	options := options.Find()
	cursor, err := collection.Find(context.Background(), primitive.D{}, options.SetLimit(100))
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	arrays := make([]*models.Array, 0)
	for cursor.Next(context.Background()) {
		arrayEntity := new(entity.ArrayEntity)
		errToDecode := cursor.Decode(arrayEntity)
		if errToDecode != nil {
			return nil, errToDecode
		}
		arr := mapper.ArrayEntityToArrayModel(arrayEntity)
		arrays = append(arrays, arr)
	}
	return arrays, nil
}

