package models

import "time"

type Array struct {
	IncomingArray  interface{}   `json:"incoming_array"`
	FlattenedArray []interface{} `json:"flattened_array"`
	Depth          int           `json:"depth"`
	Date           time.Time     `json:"date"`
}
