package array

import (
	"fmt"
	"reflect"
	"tangelo_api_flatten/app/domain/models"
	utils "tangelo_api_flatten/app/shared/utilsFunctions"
	"time"
)

func (a arrayUsesCases) FlatArray(inputArray interface{}) ([]interface{}, int) {

	startDepth := 0
	mapDepth := make(map[int]int)
	flattenArr := utils.FlatArr([]interface{}{}, startDepth, reflect.ValueOf(inputArray))
	depthMap := utils.MaxDepthOfArray(inputArray, 0, mapDepth)
	maxDepth := utils.SearchMaxIntOfMap(depthMap)

	newArr := &models.Array{
		IncomingArray:  inputArray,
		FlattenedArray: flattenArr,
		Depth:          maxDepth,
		Date:           time.Now(),
	}

	err := a.arrRepository.SaveFlattenRequest(newArr)
	if err != nil{
		fmt.Println("error inserting data")
	}
	return flattenArr, maxDepth
}
