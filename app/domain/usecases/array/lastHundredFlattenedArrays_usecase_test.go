package array

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_arrayUsesCases_LastHundredFlattenedArrays(t *testing.T) {

	t.Run("LastHundredFlattenedArrays repo function OK", func(t *testing.T) {
		mockRepo := new(arrayMockRepository)
		mockRepo.On("GetLastHundredRequest").Return(getLastHundredFlattenedStub(),nil)
		u := NewArrayUsesCases(mockRepo)
		newArray, err := u.LastHundredFlattenedArrays()
		assert.Equal(t, err,nil, "no error")
		assert.NotNil(t, newArray)
	})
}
