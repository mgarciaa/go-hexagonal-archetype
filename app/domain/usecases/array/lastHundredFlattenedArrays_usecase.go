package array

import (
	"errors"
	"tangelo_api_flatten/app/domain/models"
)

func (a *arrayUsesCases) LastHundredFlattenedArrays() ([]*models.Array, error) {

	flattenArr, err := a.arrRepository.GetLastHundredRequest()
	if err!=nil{
		return nil, errors.New("could not get hundred arrays")
	}
	return flattenArr, nil
}
