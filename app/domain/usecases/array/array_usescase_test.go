package array

import (
	"github.com/stretchr/testify/mock"
	"tangelo_api_flatten/app/domain/models"
	"time"
)

type arrayMockRepository struct {
	mock.Mock
}

func (m *arrayMockRepository) SaveFlattenRequest(newArr *models.Array) error {
	args := m.Called(newArr)
	return args.Error(0)
}
func (m *arrayMockRepository) GetLastHundredRequest() ([]*models.Array, error) {
	args := m.Called()
	if args.Get(0) == nil {
		return nil, nil
	}
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).([]*models.Array), args.Error(1)
}

func getLastHundredFlattenedStub() []*models.Array {
	arrs := make([]*models.Array, 0)
	arrs = append(arrs, getResultFlattenStub())
	return arrs
}

func getResultFlattenStub() *models.Array {

	inArr :=  []interface{}{1, 2, []int{3, 4}}
	flatArr := []interface{}{1, 2, 3, 4}
	clock := time.Date(
		2021, 11, 17, 20, 34, 58, 651387237, time.UTC)

	return &models.Array{
		IncomingArray:  inArr,
		FlattenedArray: flatArr,
		Depth:          1,
		Date:           clock,
	}
}