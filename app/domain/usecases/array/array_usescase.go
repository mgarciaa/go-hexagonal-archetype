package array

import (
	"tangelo_api_flatten/app/domain/repository"
)

type arrayUsesCases struct {
	arrRepository repository.ArrayRepository
}

func NewArrayUsesCases(arrRepository repository.ArrayRepository) *arrayUsesCases {
	return &arrayUsesCases{arrRepository: arrRepository}
}