package array

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

func Test_arrayUsesCases_FlatArray(t *testing.T) {
	t.Run("FlatArray repo function OK", func(t *testing.T) {
		mockRepo := new(arrayMockRepository)
		mockRepo.On("SaveFlattenRequest", mock.Anything).Return(nil)

		u := NewArrayUsesCases(mockRepo)
		flatArr := []interface{}{1, 2, []int{3, 4}}
		newArray, v := u.FlatArray(flatArr)

		assert.NotZerof(t, v, "no zero")
		assert.NotNil(t, newArray)

	})
}
