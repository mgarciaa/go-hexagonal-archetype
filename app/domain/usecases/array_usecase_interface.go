package usecases

import "tangelo_api_flatten/app/domain/models"

type ArrayUseCase interface {
	FlatArray(inputArray interface{}) ([]interface{}, int)
	LastHundredFlattenedArrays() ([]*models.Array, error)
}
