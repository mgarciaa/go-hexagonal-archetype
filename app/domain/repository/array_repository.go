package repository

import "tangelo_api_flatten/app/domain/models"

type ArrayRepository interface {
	SaveFlattenRequest(newArr *models.Array) error
	GetLastHundredRequest() ([]*models.Array, error)
}
